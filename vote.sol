// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract Voting {
    struct Candidate {
        uint256 id;
        string name;
        uint256 voteCount;
    }

    mapping(address => bool) public voters;
    mapping(uint256 => Candidate) public candidates;
    uint256 public totalCandidates;
    uint256 public totalVoters;

    event VoteCasted(address indexed voter, uint256 indexed candidateId);

    constructor() {
        totalCandidates = 0;
        totalVoters = 0;
    }

    function addCandidate(string memory _name) public {
        totalCandidates++;
        candidates[totalCandidates] = Candidate(totalCandidates, _name, 0);
    }

    function vote(uint256 _candidateId) public {
        require(!voters[msg.sender], "Already voted");
        require(_candidateId > 0 && _candidateId <= totalCandidates, "Invalid candidate");

        candidates[_candidateId].voteCount++;
        voters[msg.sender] = true;
        totalVoters++;

        emit VoteCasted(msg.sender, _candidateId);
    }
}
