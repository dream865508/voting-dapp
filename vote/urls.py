from django.urls import path
from vote import views

urlpatterns = [
    path('', views.home, name='home'),
    path('register/', views.register, name='register'),
    path('vote/', views.vote, name='vote'),
    path('result/', views.result, name='result'),
]
