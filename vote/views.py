from django.shortcuts import render
from web3 import Web3

w3 = Web3(Web3.HTTPProvider('http://localhost:8545'))  # Connect to the local Ethereum node

contract_address = '0x123456789...'  # Address of the deployed Voting contract
contract_abi = [...]  # ABI of the Voting contract

def home(request):
    return render(request, 'home.html')

def register(request):
    if request.method == 'POST':
        name = request.POST['name']
        address = request.POST['address']

        # Store the user registration data in the blockchain or database

    return render(request, 'register.html')

def vote(request):
    if request.method == 'POST':
        candidate_id = int(request.POST['candidate'])

        # Connect to the Voting contract
        contract = w3.eth.contract(address=contract_address, abi=contract_abi)

        # Call the vote function on the contract
        contract.functions.vote(candidate_id).transact()

    return render(request, 'vote.html')

def result(request):
    # Connect to the Voting contract
    contract = w3.eth.contract(address=contract_address, abi=contract_abi)

    # Get the total number of voters and candidate vote counts
    total_voters = contract.functions.totalVoters().call()
    candidate_votes = []
    for i in range(1, contract.functions.totalCandidates().call() + 1):
        candidate = contract.functions.candidates(i).call()
        candidate_votes.append((candidate['name'], candidate['voteCount']))

    return render(request, 'result.html', {'total_voters': total_voters, 'candidate_votes': candidate_votes})
